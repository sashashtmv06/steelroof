package com.sashashtmv.applicationforbuilders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.sashashtmv.applicationforbuilders.fragments.AboutUsFragment;
import com.sashashtmv.applicationforbuilders.fragments.CalculateFragment;
import com.sashashtmv.applicationforbuilders.fragments.EnterFragment;
import com.sashashtmv.applicationforbuilders.fragments.InformationFragment;
import com.sashashtmv.applicationforbuilders.fragments.RegistrationFragment;
import com.sashashtmv.applicationforbuilders.fragments.RuleFragment;
import com.sashashtmv.applicationforbuilders.fragments.SlopePatternsFragment;
import com.sashashtmv.applicationforbuilders.fragments.StartFragment;

public class MainActivity extends AppCompatActivity implements StartFragment.Callbacks, RegistrationFragment.OnFragmentInteractionListener,
                                                            EnterFragment.OnFragmentInteractionListener, SlopePatternsFragment.OnFragmentInteractionListener,
        InformationFragment.OnFragmentInteractionListener {
    FragmentManager mFragmentManager;
    StartFragment startFragment;
    AboutUsFragment aboutUsFragment;
    RuleFragment ruleFragment;
    EnterFragment enterFragment;
    RegistrationFragment registrationFragment;
    InformationFragment informationFragment;
    CalculateFragment calculateFragment;
    SlopePatternsFragment slopePatternsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFragmentManager = getSupportFragmentManager();
        startFragment = StartFragment.newInstance();
        mFragmentManager.beginTransaction()
                .replace(R.id.container, startFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreatEnterFragment() {
        enterFragment = EnterFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, enterFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreatAboutUsFragment() {
        aboutUsFragment = AboutUsFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, aboutUsFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreatRegistrationFragment() {
        registrationFragment = RegistrationFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, registrationFragment)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onCreatRuleFragment() {
        ruleFragment = RuleFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, ruleFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onFragmentInteraction() {
        informationFragment = InformationFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, informationFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateSlopePatternsFragment() {
        slopePatternsFragment = SlopePatternsFragment.newInstance(this);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, slopePatternsFragment)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onResultFragmentInteraction() {

    }

    @Override
    public void onScopePatterns(int i) {

    }
}
