package com.sashashtmv.applicationforbuilders.model;

public class User {
    private String mail;
    private String telephone;
    private String name;
    private String lastName;
    private String parole;

    public User(String mail, String telephone, String name, String lastName, String parole) {
        this.mail = mail;
        this.telephone = telephone;
        this.name = name;
        this.lastName = lastName;
        this.parole = parole;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getParole() {
        return parole;
    }

    public void setParole(String parole) {
        this.parole = parole;
    }
}
