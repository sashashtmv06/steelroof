package com.sashashtmv.applicationforbuilders.api;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("phone")
    public String phone;
    @SerializedName("password")
    public String password;
    @SerializedName("name")
    public String name;
    @SerializedName("surname")
    public String surname;
    @SerializedName("email")
    public String email;

    public LoginResponse(String phone, String password, String name, String surname, String email) {
        this.phone = phone;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }
}

