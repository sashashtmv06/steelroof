package com.sashashtmv.applicationforbuilders.api;

import java.util.List;

import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {
    //PreferenceHelper mPreferenceHelper = PreferenceHelper.getInstance();
//    @Headers("Content-Type: application/json")
    @FormUrlEncoded
    @POST("api/register")
    Call<Object> createUser(
            @Field("phone") String phone,
            @Field("password") String password,
            @Field("name") String name,
            @Field("surname") String surname,
            @Field("email") String email);
//    Call<AddEmailResult> addEmail(@Body List<String> emails);
//    Call getData(@Query("phone") String phone,
//                           @Query("password") String password,
//                           @Query("name") String name,
//                           @Query("surname") String surname,
//                           @Query("email") String email);
//    Call<LoginResponse> createUser(@Body LoginResponse login);
//    Call<Object> createUser(@Body String body);
//    Call<ResponseData> createUser(@Body RequestBody body);

    //
    @Headers("Content-Type: application/json")
    @POST("api/login_check")
    Call<Object> createValidation(@Body String token);

}
