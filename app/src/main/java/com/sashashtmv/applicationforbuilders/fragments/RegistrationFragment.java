package com.sashashtmv.applicationforbuilders.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sashashtmv.applicationforbuilders.MainActivity;
import com.sashashtmv.applicationforbuilders.R;
import com.sashashtmv.applicationforbuilders.api.APIClient;
import com.sashashtmv.applicationforbuilders.api.APIInterface;

import com.sashashtmv.applicationforbuilders.model.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class RegistrationFragment extends Fragment {

    private EditText mName;
    private EditText mLastName;
    private EditText mTelephone;
    private EditText mPassword;

    private EditText mEmail;
    private Button mNext;

    static Context mContext;

    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;


    private OnFragmentInteractionListener mListener;

    public RegistrationFragment() {
        // Required empty public constructor
    }

    public static RegistrationFragment newInstance(Context context) {
        RegistrationFragment fragment = new RegistrationFragment();
        mContext = context;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration, container, false);

        mTelephone = view.findViewById(R.id.et_telephone);
        mPassword = view.findViewById(R.id.et_parole);
        mName = view.findViewById(R.id.et_name);
        mLastName = view.findViewById(R.id.et_last_name);
        mEmail = view.findViewById(R.id.et_mail);
        mNext = view.findViewById(R.id.bt_next);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        mNext.setOnClickListener(mOnEnterClickListener);
        // Inflate the layout for this fragment
        return view;
    }

    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (checkValidation()) {
                if (CommonMethod.isNetworkAvailable(mContext))
                    loginRetrofit2Api(mTelephone.getText().toString(), mPassword.getText().toString(),
                            mName.getText().toString(), mLastName.getText().toString(), mEmail.getText().toString());
                else
                    CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
            }
//            else
//                showMessage(R.string.login_input_error);
        }
    };

    private void showMessage(@StringRes int string) {
        Toast.makeText(mContext, string, Toast.LENGTH_LONG).show();
    }

    private void loginRetrofit2Api(String phone, String password, String name, String lastName, String email) {
        Log.i("TAG", "loginRetrofit2Api: - " + name + " " + password);

        try {
//            final LoginResponse login = new LoginResponse(phone, password,name,lastName,email);
//            Call<LoginResponse> call1 = apiInterface.createUser(login);
//            call1.enqueue(new Callback<LoginResponse>() {
//                @Override
//                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
//                    Log.i(TAG, "onResponse: " + response.code() + response.body());
//                }
//
//                @Override
//                public void onFailure(Call<LoginResponse> call, Throwable t) {
//
//                }


//            JSONObject paramObject = new JSONObject();
//            paramObject.put("phone", phone);
//            paramObject.put("password", password);
//            paramObject.put("name", name);
//            paramObject.put("surname", lastName);
//            paramObject.put("email", email);
            Call<Object> call1 = apiInterface.createUser(phone, password,name,lastName,email);
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: registration" + response.code() + response.body());
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
                            boolean access = object.getBoolean("status");
//                            String token = object.getString("access_token");
                            String message = object.getString("message");
                            if (access) {
//                                mPreferenceHelper.putString("access_token", token);
                                onButtonPressed();
                                //startActivity(new Intent(AuthActivity.this, MainActivity.class));
                            }
                            Log.e("TAG", "response 33: " + access + message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {
                        Toast.makeText(mContext, "Ваш номер телефона или почта уже используются, введите другие данные", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + " - faile");
                }
            });
        }catch (Exception e){

        }
    }


    public boolean checkValidation() {

        if (mName.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Имя нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mPassword.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Пароль нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mPassword.getText().toString().trim().length() < 7) {
            CommonMethod.showAlert("Пароль не может быть меньше 7 символов", (MainActivity) mContext);
            return false;
        } else if (mTelephone.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Телефон нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mLastName.getText().toString().trim().equals("")) {
            CommonMethod.showAlert("Фамилию нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        }
//        else if (mEmail.getText().toString().trim().equals("")) {
//            CommonMethod.showAlert("Пароль нельзя оставлять пустым", (MainActivity) mContext);
//            return false;
//        }
        return true;
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            try {
                builder.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }
}
