package com.sashashtmv.applicationforbuilders.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sashashtmv.applicationforbuilders.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SlopePatternsFragment extends Fragment {

    private static Context mContext;
    private OnFragmentInteractionListener mListener;
    public SlopePatternsFragment() {
        // Required empty public constructor
    }

    public static SlopePatternsFragment newInstance(Context context) {
        SlopePatternsFragment fragment = new SlopePatternsFragment();
        mContext = context;

        return fragment;
    }

    public interface OnFragmentInteractionListener {

        void onResultFragmentInteraction();
        void onScopePatterns(int i);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_slope_patterns, container, false);
    }

    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onResultFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
