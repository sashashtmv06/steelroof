package com.sashashtmv.applicationforbuilders.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.sashashtmv.applicationforbuilders.MainActivity;
import com.sashashtmv.applicationforbuilders.R;
import com.sashashtmv.applicationforbuilders.api.FileUploadService;
import com.sashashtmv.applicationforbuilders.api.ServiceGenerator;
import com.sashashtmv.applicationforbuilders.model.PreferenceHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static android.content.ContentValues.TAG;
import static android.content.Context.PRINT_SERVICE;

public class CalculateFragment extends Fragment {

    private EditText mText;
    private Button mUpload;
    static Context mContext;
    File file;


    private OnFragmentInteractionListener mListener;
    private PreferenceHelper mPreferenceHelper;

    public CalculateFragment() {
        // Required empty public constructor
    }

    public static CalculateFragment newInstance(Context context) {
        CalculateFragment fragment = new CalculateFragment();
        mContext = context;
        return fragment;
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calculate, container, false);

        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();

        mText = view.findViewById(R.id.et_email);
        mUpload = view.findViewById(R.id.bt_upload);

        mUpload.setOnClickListener(mOnEnterClickListener);
        // Inflate the layout for this fragment
        return view;
    }

    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            printPDF(view);
        String uploadText = mText.getText().toString();
        writeToFile(uploadText, getContext());

            try {
                uploadFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            if (checkValidation()) {
//                if (EnterFragment.CommonMethod.isNetworkAvailable(mContext))
//                    loginRetrofit2Api(mTelephone.getText().toString(), mPassword.getText().toString());
//                else
//                    EnterFragment.CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
//            }
//            else
//                showMessage(R.string.login_input_error);
        }
    };

    private void writeToFile(String data,Context context) {
        try {
            File path = context.getFilesDir();
            file = new File( path,"my-file-name.pdf");
            FileOutputStream stream = new FileOutputStream(file);

                stream.write(data.getBytes());

                stream.close();

//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
//            outputStreamWriter.write(data);
            Log.v("Upload", "success");
//            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void uploadFile(final File file) throws IOException {
        // create upload service client
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);
        int length = (int) file.length();

        byte[] bytes = new byte[length];

        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            in.read(bytes);
        }
         finally {
            in.close();
        }

        String contents = new String(bytes);

        Log.e("Exception", "File : " + file.toString() + " " + contents);
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<Object> call = service.upload("Bearer " + mPreferenceHelper.getString("access_token"), body);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call,
                                   Response<Object> response) {
                Log.i(TAG, "onResponse: " + response.code() + response.body() + "  -  " + mPreferenceHelper.getString("access_token"));
                Log.v("Upload", "success");
                file.delete();
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    public void printPDF(View view) {
        PrintManager printManager = (PrintManager) getActivity().getSystemService(PRINT_SERVICE);
        printManager.print("print_any_view_job_name", new ViewPrintAdapter(getActivity(),
                view.findViewById(R.id.rl_calc)), null);
    }

    public class ViewPrintAdapter extends PrintDocumentAdapter {

        private PrintedPdfDocument mDocument;
        private Context mContext;
        private View mView;

        public ViewPrintAdapter(Context context, View view) {
            mContext = context;
            mView = view;
        }

        @Override
        public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes,
                             CancellationSignal cancellationSignal,
                             LayoutResultCallback callback, Bundle extras) {

            mDocument = new PrintedPdfDocument(mContext, newAttributes);

            if (cancellationSignal.isCanceled()) {
                callback.onLayoutCancelled();
                return;
            }

            PrintDocumentInfo.Builder builder = new PrintDocumentInfo
                    .Builder("print_output.pdf")
                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                    .setPageCount(1);

            PrintDocumentInfo info = builder.build();
            callback.onLayoutFinished(info, true);
            Log.i(TAG, "onLayout: path - " + info.getName());
        }

        @Override
        public void onWrite(PageRange[] pages, ParcelFileDescriptor destination,
                            CancellationSignal cancellationSignal,
                            WriteResultCallback callback) {

            // Start the page
            PdfDocument.Page page = mDocument.startPage(0);
            // Create a bitmap and put it a canvas for the view to draw to. Make it the size of the view
            Bitmap bitmap = Bitmap.createBitmap(mView.getWidth(), mView.getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            mView.draw(canvas);
            // create a Rect with the view's dimensions.
            Rect src = new Rect(0, 0, mView.getWidth(), mView.getHeight());
            // get the page canvas and measure it.
            Canvas pageCanvas = page.getCanvas();
            float pageWidth = pageCanvas.getWidth();
            float pageHeight = pageCanvas.getHeight();
            // how can we fit the Rect src onto this page while maintaining aspect ratio?
            float scale = Math.min(pageWidth/src.width(), pageHeight/src.height());
            float left = pageWidth / 2 - src.width() * scale / 2;
            float top = pageHeight / 2 - src.height() * scale / 2;
            float right = pageWidth / 2 + src.width() * scale / 2;
            float bottom = pageHeight / 2 + src.height() * scale / 2;
            RectF dst = new RectF(left, top, right, bottom);

            pageCanvas.drawBitmap(bitmap, src, dst, null);
            mDocument.finishPage(page);

            try {
                mDocument.writeTo(new FileOutputStream(
                        destination.getFileDescriptor()));
            } catch (IOException e) {
                callback.onWriteFailed(e.toString());
                return;
            } finally {
                mDocument.close();
                mDocument = null;
            }
            callback.onWriteFinished(new PageRange[]{new PageRange(0, 0)});
        }
    }


//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
