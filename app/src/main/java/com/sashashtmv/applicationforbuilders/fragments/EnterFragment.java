package com.sashashtmv.applicationforbuilders.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sashashtmv.applicationforbuilders.MainActivity;
import com.sashashtmv.applicationforbuilders.R;
import com.sashashtmv.applicationforbuilders.api.APIClient;
import com.sashashtmv.applicationforbuilders.api.APIInterface;
import com.sashashtmv.applicationforbuilders.model.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class EnterFragment extends Fragment {

    private String telephoneMail;
    private String parole;
    private EditText mTelephone;
    private EditText mPassword;
    private Button mNext;

    private APIInterface apiInterface;
    private PreferenceHelper mPreferenceHelper;
    static Context mContext;

    private OnFragmentInteractionListener mListener;

    public EnterFragment() {
        // Required empty public constructor
    }


    public static EnterFragment newInstance(Context context) {
        EnterFragment fragment = new EnterFragment();
        mContext = context;

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter, container, false);

        PreferenceHelper.getInstance().init(mContext);
        mPreferenceHelper = PreferenceHelper.getInstance();

        mTelephone = view.findViewById(R.id.et_email);
        mPassword = view.findViewById(R.id.et_parole);
        mNext = view.findViewById(R.id.bt_next);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        mNext.setOnClickListener(mOnEnterClickListener);
        // Inflate the layout for this fragment
        return view;
    }

    private View.OnClickListener mOnEnterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onButtonPressed();

            if (checkValidation()) {
                if (EnterFragment.CommonMethod.isNetworkAvailable(mContext))
                    loginRetrofit2Api(mTelephone.getText().toString(), mPassword.getText().toString());
                else
                    EnterFragment.CommonMethod.showAlert("Internet Connectivity Failure", (MainActivity) mContext);
            }
//            else
//                showMessage(R.string.login_input_error);
        }
    };

    private void showMessage(@StringRes int string) {
        Toast.makeText(mContext, string, Toast.LENGTH_LONG).show();
    }

    private void loginRetrofit2Api(String phone, String password) {
        Log.i("TAG", "loginRetrofit2Api: - " + phone + " " + password);
        List<String> list = new ArrayList<>();
        list.add(phone);
        list.add(password);

        try {

            JSONObject paramObject = new JSONObject();
            paramObject.put("username", phone);
            paramObject.put("password", password);

            Call<Object> call1 = apiInterface.createValidation(paramObject.toString());
            call1.enqueue(new Callback<Object>() {

                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    Log.i(TAG, "onResponse: " + response.code() + response.body());
                    JSONObject object = null;
                    if (response.isSuccessful() && response.body() != null) {
                        try {
                            object = new JSONObject(new Gson().toJson(response.body()));
//                            boolean access = object.getBoolean("status");
//                            String token = object.getString("access_token");
                            String token = object.getString("token");
                    Log.i(TAG, "onResponse: " + token);
                            if (token != null && token.length() > 0) {
                                mPreferenceHelper.putString("access_token", token);
                                onButtonPressed();
                            }
                            Log.e("TAG", "response 33: " + token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(mContext, "Введенные данные не верны", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.i(TAG, "onResponse: " + " - faile");
                }
            });
        } catch (Exception e) {

        }
    }


    public boolean checkValidation() {

        if (mPassword.getText().toString().trim().equals("")) {
            EnterFragment.CommonMethod.showAlert("Пароль нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mTelephone.getText().toString().trim().equals("")) {
            EnterFragment.CommonMethod.showAlert("Телефон нельзя оставлять пустым", (MainActivity) mContext);
            return false;
        } else if (mPassword.getText().toString().trim().length() < 7) {
            EnterFragment.CommonMethod.showAlert("Пароль не может быть меньше 7 символов", (MainActivity) mContext);
            return false;
        }
            return true;
    }

    public static class CommonMethod {

        public static boolean isNetworkAvailable(Context ctx) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public static void showAlert(String message, Activity context) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            try {
                builder.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EnterFragment.OnFragmentInteractionListener) {
            mListener = (EnterFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction();
    }
}
