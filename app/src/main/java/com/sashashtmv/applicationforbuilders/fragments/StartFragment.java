package com.sashashtmv.applicationforbuilders.fragments;


import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sashashtmv.applicationforbuilders.R;


public class StartFragment extends Fragment {
    Toolbar toolbar;
    private Callbacks mCallbacks;
    private Button mEnter;
    private Button mAboutUs;
    private Button mNewUser;
    private Button mRule;


    public StartFragment() {
        // Required empty public constructor
    }

    public interface Callbacks{
        void onCreatEnterFragment();
        void onCreatAboutUsFragment();
        void onCreatRegistrationFragment();
        void onCreatRuleFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks)activity;
    }

    public static StartFragment newInstance() {
        StartFragment fragment = new StartFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);
        setHasOptionsMenu(true);

        toolbar = view.findViewById(R.id.toolbar_fragment);
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        activity.setSupportActionBar(toolbar);

        mAboutUs = view.findViewById(R.id.bt_about_us);
        mEnter = view.findViewById(R.id.bt_enter);
        mNewUser = view.findViewById(R.id.bt_new_user);
        mRule = view.findViewById(R.id.bt_rule);

        mAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallbacks.onCreatAboutUsFragment();
            }
        });
        mRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallbacks.onCreatRuleFragment();
            }
        });
        mEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallbacks.onCreatEnterFragment();
            }
        });
        mNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallbacks.onCreatRegistrationFragment();
            }
        });
        //activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        //activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.call_toolbar);
        activity.getSupportActionBar().setTitle(getString(R.string.app_for_builders));
        return view;
    }

}
