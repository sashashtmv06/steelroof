package com.sashashtmv.applicationforbuilders.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sashashtmv.applicationforbuilders.MainActivity;
import com.sashashtmv.applicationforbuilders.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformationFragment extends Fragment {


    private static Context mContext;
    private OnFragmentInteractionListener mListener;
    private Button next;

    public InformationFragment() {
        // Required empty public constructor
    }

    public static InformationFragment newInstance(Context context) {
        InformationFragment fragment = new InformationFragment();
        mContext = context;

        return fragment;
    }

    public interface OnFragmentInteractionListener {

        void onCreateSlopePatternsFragment();
//        void onScopePatterns(int i);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_information, container, false);

        next = view.findViewById(R.id.bt_next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonPressed();
            }
        });
        return view;
    }

    public void onButtonPressed() {
        if (mListener != null) {
            mListener.onCreateSlopePatternsFragment();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentRegistrationListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
