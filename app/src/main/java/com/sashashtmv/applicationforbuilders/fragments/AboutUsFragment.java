package com.sashashtmv.applicationforbuilders.fragments;


import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sashashtmv.applicationforbuilders.MainActivity;
import com.sashashtmv.applicationforbuilders.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment {
    private TextView title;
    private FragmentManager mFragmentManager;
    private static Context mContext;


    public AboutUsFragment() {
        // Required empty public constructor
    }

    public static AboutUsFragment newInstance(Context context) {
        AboutUsFragment fragment = new AboutUsFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_about_us, container, false);

        SpannableString ss = new SpannableString("Steel Roof динамически развивающаяся компания цель которой упростить работу строителей и внедрить новые современные технологии в повседневную жизнь строителя.\n\n" +
                "Мы создали это приложение с целью облегчения просчетов и учета материалов при конструировании и создании крыш любой сложности.\n\n" +
                "Теперь Вы можете не только рассчитывать на правильные рассчеты в вашей работе, а еще и на получение высокого результата и экономии времени.\n\n" +
                "Правила пользования приложением описаны в нашем разделе Правила пользования.\n\n" +
                "Кроме этого, Вы будете получать новые предложения по усовершенствованию программы, а также все новости относительно работы приложения на Вашу почту");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                //Activity activity = (Activity)mContext;
//                Toast.makeText(activity, "Start Sign up activity",
//                        Toast.LENGTH_SHORT).show();

                mFragmentManager = getFragmentManager();
                RuleFragment ruleFragment = RuleFragment.newInstance(mContext);
                mFragmentManager.beginTransaction()
                        .replace(R.id.container, ruleFragment)
                        .addToBackStack(null)
                        .commit();

            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 485, 507, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        title =(TextView) view.findViewById(R.id.tv_hello);
        title.setText(ss);
        title.setMovementMethod(LinkMovementMethod.getInstance());
        title.setHighlightColor(Color.TRANSPARENT);

        return view;
    }

}
