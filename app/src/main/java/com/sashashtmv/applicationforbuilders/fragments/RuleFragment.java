package com.sashashtmv.applicationforbuilders.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sashashtmv.applicationforbuilders.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RuleFragment extends Fragment {

    private static Context mContext;

    public RuleFragment() {
        // Required empty public constructor
    }

    public static RuleFragment newInstance(Context context) {
        RuleFragment fragment = new RuleFragment();
        mContext = context;

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rule, container, false);
        // Inflate the layout for this fragment
        return view;
    }

}
